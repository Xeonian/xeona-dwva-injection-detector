# DWVA Challenge

## This Solution

With the given time constraint of 2-3 hours, I was unfortunately unable to get a great deal done with regards to this 
challenge. At time of writing, I have implemented what I intend to be a module for crawling a website, starting at its
index and navigating all hypertext references recursively, and returning the pages and page sources for those pages that
meet a requested pattern; and a module that, given a page source, will identify all forms on that page, and attempt to
submit those forms with a payload that would trigger a SQL injection attack, returning a boolean indicating whether any
such attack on the page was successful.

I have been unable to test these modules, either using automated tests or manually. I do not consider this to be an 
accurate reflection of my work ethic, however I decided that the phrasing of the test as given implied it would be 
preferable to be true to the allotted time limit, than to spend an excessive amount of time finessing the solution. In 
addition, this solution has not yet been made to target the DWVA pages, and therefore has no accommodation for custom 
authentication or cookies, which would be required for a complete solution.

### Proposed improvements

Given additional time, this simple command line program would be extended with the following features:

* A main method, accepting as argument a root URL of a target domain, as well as a (potentially optional) regex pattern 
to be applied to paths, which would tie the functionality of the implemented modules together
* Unit tests, validating the behaviour of the modules against mocks, using `pytest`
* Feature tests, which would prepare a fixed environment (most likely using the official DWVA docker image) and executes
the application to verify the integrated behaviour, using `behave` as the feature test runner
* Continuous integration, such that tests are perform frequently in an automated fashion to validate the quality of code
before it is able to merge into the primary branch.

## Broader Architectural Considerations

This implementation represents a simplistic command line tool, which seemed an appropriate approach given the timescale
involved, and as a demonstration of the algorithms involved. However, the challenge requests an architectural design 
with consideration to a more scaleable approach. I personally have never become well acquainted with any diagramming 
tools, so I thought it best to express my design considerations in textual form.

The crawler and the injection would form two different applications. An endpoint would be provided, most likely in the 
form of a REST API, for an authenticated user to submit a request to crawl a website, supplying both a root URL and 
rules for constraining the scope of searched pages. The direct result of this request could be the simple insert of a 
record/document into the database, specifying the requested parameters, and demarking the page as yet unexamined. 
(MongoDB is a natural choice based on the provided challenge document, however any storage backend that provides 
scaleable IOPS ie via sharding would be appropriate). A scaleable pool of crawlers can periodically poll the database, 
seeking records for pages that are unclaimed an unexamined, staking a claim on a record and performing a scrape to seek
other pages. Newly identified pages - those that do not yet have an entry in the database - can be inserted similarly, 
giving other crawlers an opportunity to claim the work. If the pool is operating with failover, the crawlers could 
favour records for which they already have a claim, meaning that jobs are automatically reclaimed by crawlers upon 
restart. If the crawlers exist as part of a scaling pool, the entity responsible for scaling could release any 
uncompleted claims when crawlers are removed from the pool, freeing them for the remaining crawlers to complete in their 
stead.

The SQL injection testers could operate in a similar fashion, with a pool of testing applications that poll and claim 
pages as they become available, writing their claims and then their results back to the database as they progress. 
Depending on the storage backend used, we could track this progress independently as either a related collection, or an
embedded document associated with the page - in this fashion, so long as each check is given a unique key, multiple 
checks - either SQL injection variants or checks for different forms of vulnerability entirely - can all operate 
concurrently, each staking claims and writing results for their particular class of test. In this way, the pool of 
potential tests can grow as the platform develops, and the increase in computational effort brought about by this growth
can be handled by horizontal scaling of resource, thereby keeping the time taken to complete full testing within the 
same order of magnitude.