from setuptools import setup, find_namespace_packages

setup(
    name='xeona.dwva-injection-detector',
    version="0.0.1",
    packages=find_namespace_packages(),
    python_requires='>=3.8',
    install_requires=[
        'aiohttp',
        'beautifulsoup4',
        'attrs',
    ],
    tests_require=[
        'pytest',
    ]
)
