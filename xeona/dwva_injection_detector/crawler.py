import asyncio
import urllib.parse
from typing import Pattern, Set, Callable, Awaitable, Generator

from aiohttp import ClientSession
from attr import attr
from bs4 import BeautifulSoup

OnTargetCallback = Callable[[ClientSession, BeautifulSoup], Awaitable[bool]]


@attr.s
class CrawlConfig:
    client_session: ClientSession
    target_domain: str
    target_paths_pattern: Pattern
    callback: OnTargetCallback


@attr.s
class CrawlResult:
    page_uri: str
    document: BeautifulSoup


async def crawl_pages(crawl_config: CrawlConfig) -> Generator[CrawlResult, None, None]:
    def is_target_domain(url: str) -> bool:
        parsed_url = urllib.parse.urlparse(url)
        return f"{parsed_url.scheme}://{parsed_url.netloc}" == crawl_config.target_domain

    async def recurse(current_target: str, visited_pages: Set[str]) -> Generator[CrawlResult, None, None]:
        async with crawl_config.client_session.get(current_target) as resp:
            page_source = await resp.text()

        document = BeautifulSoup(page_source, 'html.parser')

        if crawl_config.target_paths_pattern.match(current_target):
            yield CrawlResult(page_uri=current_target, document=document)

        pages_to_scrape = {
            referenced_page
            for referenced_page in {
                urllib.parse.urljoin(current_target, anchor.get('href'))
                for anchor in document.find_all('a')
            }
            if referenced_page not in visited_pages
            and is_target_domain(referenced_page)
        }
        new_visited_pages = {
            *visited_pages,
            *pages_to_scrape
        }

        async for recurse_result in asyncio.gather(*[recurse(page_to_scrape, new_visited_pages)
                                                     for page_to_scrape in pages_to_scrape]):
            async for crawl_result in recurse_result:
                yield crawl_result

    async for result in recurse(crawl_config.target_domain, crawl_config):
        yield result
