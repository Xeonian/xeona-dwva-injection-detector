import asyncio
import uuid

from aiohttp import ClientSession
from bs4 import BeautifulSoup, Tag
import urllib.parse

_SUBMITTED_INPUT_TYPES = {'text'}  # TODO: Additional input types


async def check_for_sqli_vuln(client_session: ClientSession, page_uri: str, document: BeautifulSoup) -> bool:
    async def is_form_vulnerable(form: Tag) -> bool:
        submit_uri = urllib.parse.urljoin(page_uri, form.get('action'))
        submit_method = form.get('method', 'get')
        submitted_params = [
            input_element.get('name')
            for input_element in form.find_all('input')
            if input_element.get('type') in _SUBMITTED_INPUT_TYPES
        ]

        token = uuid.uuid4()
        injection_payload = f"'; SELECT '{uuid.uuid4()} AS 'Injected"

        if submit_method == 'get':
            params = {key: injection_payload for key in submitted_params}
            data = {}
        else:
            params = {}
            data = {key: injection_payload for key in submitted_params}

        async with client_session.request(submit_method, submit_uri, params=params, data=data) as response:
            response_body = await response.text()
            return str(token) in response_body

    return any(await asyncio.gather(*[is_form_vulnerable(form) for form in document.find_all('form')]))
